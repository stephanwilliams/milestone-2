package edu.gatech.oad.antlab.person;

/**
 *  A simple class for person 2
 *  returns their name and a
 *  modified string 
 *
 * @author Michael Lu
 * @version 1.1
 */
import java.util.Random;
public class Person2 {
    /** Holds the persons real name */
    private String name;
	 	/**
	 * The constructor, takes in the persons
	 * name
	 * @param pname the person's real name
	 */
	 public Person2(String pname) {
	   name = pname;
	 }
	/**
	 * This method should take the string
	 * input and return its characters in
	 * random order.
	 * given "gtg123b" it should return
	 * something like "g3tb1g2".
	 *
	 * @param input the string to be modified
	 * @return the modified string
	 */
	private String calc(String input) {
	  //Person 2 put your implementation here
	   String tempname = "";
	  int size = input.length();
	  Random rand = new Random();

	  while(size > 0){
		int x = rand.nextInt(size);
		tempname += input.substring(x, x+1);
		String replacementName = "";
		for(int y = 0; y < input.length(); y++){
			if(y == x){
			}
			else{

				replacementName += input.substring(y,y+1);	
			}
		}
		input = replacementName;
		size--;
	  }
	  return tempname;
	}
	/**
	 * Return a string rep of this object
	 * that varies with an input string
	 *
	 * @param input the varying string
	 * @return the string representing the 
	 *         object
	 */
	public String toString(String input) {
	  return name + calc(input);
	}
}
